FROM debian:buster

RUN apt-get update && apt-get -y install curl git

RUN curl -o /tmp/gitlab-runner_amd64.deb \
    https://gitlab.com/ubarbaxor/gitlab-runner/-/jobs/270337347/artifacts/raw/out/deb/gitlab-runner_amd64.deb

# "Fails" for lack of systemd - return true to avoid breaking docker build
RUN dpkg -i /tmp/gitlab-runner_amd64.deb || true

CMD [ "tail", "-f", "/dev/null" ]
