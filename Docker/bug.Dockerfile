FROM debian:buster

RUN apt-get update && apt-get -y install curl

RUN curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash

RUN apt-get -y install gitlab-runner

CMD [ "tail", "-f", "/dev/null" ]
