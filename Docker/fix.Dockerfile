FROM runner:bug

COPY    ./src/diff /tmp/diff
RUN     patch /usr/share/gitlab-runner/post-install /tmp/diff
RUN     userdel -r gitlab-runner && dpkg-reconfigure gitlab-runner