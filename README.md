# gitlab-runner-test-3849

Test for issue #3849 on https://gitlab.com/gitlab-org/gitlab-runner

Simple as `./run.sh`. Builds and registers 2 runners using Debian 10 "Buster".
Dockerfiles found in `./Docker`.
