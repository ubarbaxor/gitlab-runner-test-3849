#!/bin/bash

DIR=$(dirname $0)
echo "Token:"
read TOKEN

# bug: upstream
# fix: manual fix
# job: build from merge request 1523 CI
for kw in bug fix job;
do
    docker build $DIR -f $DIR/Docker/$kw.Dockerfile -t runner:$kw
    docker run -d --name $kw-runner runner:$kw
    docker exec -i $kw-runner gitlab-runner register << EOF
https://gitlab.com/
$TOKEN
$kw runner for issue #3849
runner, $kw
shell
EOF
    docker exec -d $kw-runner   \
        gitlab-runner run       \
        --user gitlab-runner    \
        --working-directory /home/gitlab-runner
done
